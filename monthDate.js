/**
 * 只能选择月日的时间选择框
 * Author:seebin
 * Email:656487723@qq.com
 */


/*
使用方法：

var options = {
  // 初始化月日值
  openInitMD: '09-01',
  // 月日触发的元素id，必填
  initEleId: '#moneyDate',
  // 最小日期的元素id
  minEleId: '#minEleId',
  // 最大日期的元素id
  maxEleId: '#maxEleId',
  // 是否点选date关闭 默认true
  isAutoClose: true,
  // 链接符号,默认 '-',可取(- / 月日)
  symbol:'-',
  // 选择天数的回调：点选天数与点击确定键,在这里面回显选中的月日到视图
  ok: function (value, month, date) { },
  // 点击清空的回调：点击清空键，在这里面清空视图显示的值
  reset: function () { },
}

new MonthDate(options).init();
*/

function MonthDate(options) {
  this.options = options;
}

MonthDate.prototype.init = function () {
  // 设置默认值
  this.options.isAutoClose = this.options.isAutoClose == undefined ? true : this.options.isAutoClose;
  this.options.minEleId = this.options.minEleId ? this.options.minEleId : '#';
  this.options.maxEleId = this.options.maxEleId ? this.options.maxEleId : '#';
  this.options.symbol = this.options.symbol ? this.options.symbol : '-';

  // 触发节点
  var eleMD = document.querySelector(this.options.initEleId), owner = this;

  // 判断月日大小
  function judgeMonthDate(firstMD, secondMD) {
    if (owner.options.symbol == '月日') {
      // 处理特殊格式
      firstMD = firstMD.replace('日', '');
      firstMD = firstMD.replace('月', owner.options.symbol);
      secondMD = secondMD.replace('日', '');
      secondMD = secondMD.replace('月', owner.options.symbol);
    }
    // firstMD > secondMD return true;
    var aFirstMD = firstMD.split(owner.options.symbol);
    var month = aFirstMD[0] * 1;
    var date = aFirstMD[1] * 1;
    // 最小可选月日
    var aSecondMD = secondMD.split(owner.options.symbol);
    var minMonth = aSecondMD[0] * 1;
    var minDate = aSecondMD[1] * 1;
    if ((month > minMonth) || ((month == minMonth) && (date >= minDate))) {
      return true;
    } else {
      return false;
    }
  }

  // 拼接选中的月日
  function spliceMonthDate(month, date) {
    if (owner.options.symbol == '月日') {
      return month + '月' + date + '日';
    } else {
      return month + owner.options.symbol + date;
    }
  }

  // 监听绑定的元素
  eleMD.addEventListener('click', function () {
    // 月日框初始化值依次为元素值、设定的初始值、默认值
    owner.options.initMD = eleMD.value || owner.options.openInitMD || '01' + owner.options.symbol + '01';

    if (owner.options.minEleId != '#') {
      var minEle = document.querySelector(owner.options.minEleId);
      owner.options.minMD = minEle.value;
      // 如果初始化月日大于开始的月日，取初始月日，否则取最小月日
      if (!judgeMonthDate(owner.options.initMD, owner.options.minMD)) {
        owner.options.initMD = owner.options.minMD;
      }
    } else {
      // 如果不存在最小月日，取初始月日为最小月日
      owner.options.minMD = '01' + owner.options.symbol + '01';
    }

    // 取出最大值
    if (owner.options.maxEleId != '#') {
      var maxEle = document.querySelector(owner.options.maxEleId);
      owner.options.maxMD = maxEle.value;
      if (!owner.options.maxMD) {
        owner.options.maxMD = '12' + owner.options.symbol + '31';
      }
    } else {
      owner.options.maxMD = '12' + owner.options.symbol + '31';
    }
    if (owner.options.symbol == '月日') {
      // 处理特殊格式
      owner.options.initMD = owner.options.initMD.replace('日', '');
      owner.options.initMD = owner.options.initMD.replace('月', owner.options.symbol);
      owner.options.minMD = owner.options.minMD.replace('日', '');
      owner.options.minMD = owner.options.minMD.replace('月', owner.options.symbol);
      owner.options.maxMD = owner.options.maxMD.replace('日', '');
      owner.options.maxMD = owner.options.maxMD.replace('月', owner.options.symbol);
    }

    // 初始化月日
    var aInitMD = owner.options.initMD.split(owner.options.symbol);
    var month = aInitMD[0] * 1;
    var date = aInitMD[1] * 1;
    // 最小可选月日
    var aMinMD = owner.options.minMD.split(owner.options.symbol);
    var minMonth = aMinMD[0] * 1;
    var minDate = aMinMD[1] * 1;
    // 最大可选月日
    var aMaxMD = owner.options.maxMD.split(owner.options.symbol);
    var maxMonth = aMaxMD[0] * 1;
    var maxDate = aMaxMD[1] * 1;

    // 先定位
    var left = getElementPageLeft(eleMD);
    var top = getElementPageTop(eleMD);

    // 再将box放到body元素的末尾，显示出来
    var html = '<div class="sb-md-title">';
    html += '<div class="sb-md-pre" id="sb-md-pre"></div>'; // sb-md-pre
    html += '<div class="sb-md-month" id="sb-md-month">';
    html += '<span id="sb-md-monthNum">12</span>月';
    html += ' </div>';// sb-md-month
    html += ' <div class="sb-md-next" id="sb-md-next"></div>';//sb-md-next
    html += '</div>'; // sb-md-title
    html += '<ul class="sb-md-date" id="sb-md-date"></ul>'; // sb-md-date
    html += '<div class="sb-md-btn">';
    html += ' <div class="sb-md-ok" id="sb-md-ok">确定</div>';
    html += ' <div class="sb-md-reset" id="sb-md-reset">清空</div>';
    html += '</div>'; // sb-md-btn
    html += '<div class="sb-md-setMonth" id="sb-md-setMonth"></div>';

    var box = document.createElement('div');
    box.id = 'sb-md-box';
    box.className = 'sb-md-box';
    box.innerHTML = html;
    document.querySelector('body').append(box);

    box.style.left = left + 'px';
    box.style.top = top + 'px';
    box.style.display = 'block';

    var eMonthNum = box.querySelector('#sb-md-monthNum');

    var eDate = box.querySelector('#sb-md-date');
    selectMonth(month);

    var eMonthWarp = box.querySelector('#sb-md-setMonth');
    var eMonthListStr = '';
    for (var i = 1; i < 13; i++) {
      if (i == month) {
        eMonthListStr += '<div class="sb-md-month-item sb-md-month-item-action">' + i + '月</div>';
      } else {
        eMonthListStr += '<div class="sb-md-month-item ' + ((i < minMonth || i > maxMonth) ? 'disabled' : '') + '">' + i + '月</div>';
      }
    }
    eMonthWarp.innerHTML = eMonthListStr;


    Array.from(eMonthWarp.querySelectorAll('div')).map(function (item) {
      item.addEventListener('click', function (e) {
        if (item.className.indexOf('disabled') != -1) {
          return;
        }
        month = (item.innerHTML.replace('月', '')) * 1;

        selectMonth(month);
        eMonthWarp.style.display = 'none';

      }, false);
    });

    // 监听确定按钮
    addEventFun('#sb-md-ok', function (e) {
      if (date == 0) {
        alert('请选择时间');
        return;
      }
      document.body.removeChild(box);
      owner.options.ok(spliceMonthDate(month, date), month, date);
    });

    // 监听清空按钮
    addEventFun('#sb-md-reset', function (e) {
      document.body.removeChild(box);
      owner.options.reset();
    });

    // 选择月份，显示对应的天数
    function selectMonth(monthValue) {
      eMonthNum.innerHTML = monthValue;
      var eDateStr = '';
      if (monthValue == 1 || monthValue == 3 || monthValue == 5 || monthValue == 7 || monthValue == 8 || monthValue == 10 || monthValue == 12) {
        // 31天
        for (var i = 1; i < 32; i++) {
          var item = ((i < minDate || i > maxDate) && (owner.options.maxEleId != '#' ? (monthValue == maxMonth) : (owner.options.minEleId != '#' ? (monthValue == minMonth) : true))) ? 'disabled' : '';
          if (date == i) {
            if (item == '') {
              eDateStr += '<li class="sb-md-action">' + i + '</li>';
            } else {
              eDateStr += '<li class="' + item + '">' + i + '</li>';
              date = 0;
            }
          } else {
            eDateStr += '<li class="' + item + '">' + i + '</li>';
          }
        }
      } else if (monthValue == 4 || monthValue == 6 || monthValue == 9 || monthValue == 11) {
        // 30天
        for (var i = 1; i < 31; i++) {
          var item = ((i < minDate || i > maxDate) && (owner.options.maxEleId != '#' ? (monthValue == maxMonth) : (owner.options.minEleId != '#' ? (monthValue == minMonth) : true))) ? 'disabled' : '';
          if (date == i) {
            if (item == '') {
              eDateStr += '<li class="sb-md-action">' + i + '</li>';
            } else {
              eDateStr += '<li class="' + item + '">' + i + '</li>';
              date = 0;
            }
          } else {
            eDateStr += '<li class="' + item + '">' + i + '</li>';
          }
        }
        if (date == 31) date = 0;
      } else {
        // 二月份 写死29天
        for (var i = 1; i < 30; i++) {
          var item = ((i < minDate || i > maxDate) && (owner.options.maxEleId != '#' ? (monthValue == maxMonth) : (owner.options.minEleId != '#' ? (monthValue == minMonth) : true))) ? 'disabled' : '';
          if (date == i) {
            if (item == '') {
              eDateStr += '<li class="sb-md-action">' + i + '</li>';
            } else {
              eDateStr += '<li class="' + item + '">' + i + '</li>';
              date = 0;
            }
          } else {
            eDateStr += '<li class="' + item + '">' + i + '</li>';
          }
        }
        if (date == 31 || date == 30) date = 0;
      }
      eDate.innerHTML = eDateStr;

      // 监听里面的li标签
      Array.from(eDate.querySelectorAll('li')).map(function (item) {
        item.addEventListener('click', function (e) {
          if (item.className.indexOf('disabled') != -1) {
            return;
          }
          date = item.innerHTML * 1;

          if (owner.options.isAutoClose) {
            if (date == 0) {
              alert('请选择时间');
              return;
            }
            document.body.removeChild(box);
            owner.options.ok(spliceMonthDate(month, e.target.innerHTML), month, e.target.innerHTML);
          } else {
            // 把所有的激活样式去掉，设置当前点的为激活样式
            eDate.childNodes.forEach(function (ele) {
              ele.className = '';
            })
            item.className = 'sb-md-action';
          }

        }, false);
      })
    }

    // 监听封装点击方法
    function addEventFun(ele, callback) {
      box.querySelector(ele).addEventListener('click', function (e) {
        callback(e)
      });
    }

    // 获取元素绝对位置
    function getElementPageLeft(element) {
      var actualLeft = element.offsetLeft;
      var parent = element.offsetParent;
      while (parent != null) {
        actualLeft += parent.offsetLeft + (parent.offsetWidth - parent.clientWidth) / 2;
        parent = parent.offsetParent;
      }
      return actualLeft;
    }

    function getElementPageTop(element) {
      var actualTop = element.offsetTop;
      var parent = element.offsetParent;
      while (parent != null) {
        actualTop += parent.offsetTop + (parent.offsetHeight - parent.clientHeight) / 2;
        parent = parent.offsetParent;
      }
      // 判断body上是否加入overflow：hidden属性，若有，还需要减去body滚动的高度
      // 解决bootstrap modal 弹出框的位置问题
      if (document.body.className.indexOf('modal-open') != -1) {
        var scrollTop = 0;
        if (document.documentElement && document.documentElement.scrollTop) {
          scrollTop = document.documentElement.scrollTop;
        }
        else if (document.body) {
          scrollTop = document.body.scrollTop;
        }
        actualTop = actualTop + scrollTop - (document.querySelector('.modal-body') ? document.querySelector('.modal-body').scrollTop : 0);
      }
      return actualTop;
    }

    // 监听左右按钮事件
    addEventFun('#sb-md-pre', function (e) {
      if (month == minMonth) return;
      if (eMonthWarp.style.display == 'block') return;
      if (month == 1) return;
      month = month - 1;
      eMonthNum.innerHTML = month;
      selectMonth(month);
    });
    addEventFun('#sb-md-next', function (e) {
      if (month == maxMonth) return;
      if (eMonthWarp.style.display == 'block') return;
      if (month == 12) return;
      month = month * 1 + 1;
      eMonthNum.innerHTML = month;
      selectMonth(month);
    });

    // 监听月份点击下拉所有的月份选择
    addEventFun('#sb-md-month', function (e) {
      Array.from(eMonthWarp.querySelectorAll('div')).map(function (item) {
        var m = item.innerHTML.replace('月', '') * 1;
        if (month == m) {
          // 如果当前节点没有激活样式，则添加
          if (item.className.indexOf('sb-md-month-item-action') == -1) {
            item.className += ' sb-md-month-item-action';
          }
          // item.className = 'sb-md-month-item sb-md-month-item-action'
        } else {
          // 如果找到了激活样式，则去掉激活样式
          if (item.className.indexOf('sb-md-month-item-action') != -1) {
            item.className = item.className.replace('sb-md-month-item-action', '');
          }
          // item.className = 'sb-md-month-item '
        }
      });
      eMonthWarp.style.display = 'block';
    })

  });

}

// 查询当前元素以及父元素
function isParent(obj, parentObj) {
  while (obj != undefined && obj != null && obj.tagName.toUpperCase() != 'HTML') {
    if (obj == parentObj) {
      return true;
    }
    obj = obj.parentNode;
  }
  return false;
}

// 监听文档的点击，关闭box
document.addEventListener('mouseup', function (e) {
  var box = document.querySelector('#sb-md-box');
  if (box && !isParent(e.target, box)) {
    if (box) document.body.removeChild(box);
  }
});

window.MonthDate = MonthDate;

