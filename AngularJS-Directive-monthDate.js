define([
  'angular',
  // 引入月日控件
  'monthDate'
], function () {

  /**
   * monthDate - 月日时间选择框
   *
   * Auther:seebin
   *
   * 使用方法：
   * 1、在现有的时间输入框样式上，即input标签上添加指令 month-date
   * 2、此外还必须写入一个唯一的id用来获取触发日期选择框的元素
   * 3、必须使用 mg-model 属性来进行双向数据绑定（该数据仅支持最多一层嵌套的对象数据类型值，或者一个基础的数据类型值）
   *
   * 栗子1：
   * <input type="text" id="birthday" ng-model="param.birthday" month-date symbol='/' readonly>
   * 栗子2：
   * <input type="text" id="minBirthday" ng-model="birthday" min-ele-id="memBirthday" month-date readonly>
   *
   *
   */
  function monthDate() {
    function initDate(scope, element, attributes) {
      if (!attributes.ngModel) {
        throw new Error('必须为该月日选择框赋值一个用于双向绑定的变量值，栗子：ng-model="birthday"');
        return;
      }
      var list;
      if (attributes.ngModel && attributes.ngModel.indexOf('.') != -1) {
        list = attributes.ngModel.split('.');
      }
      if (list && list.length > 2) {
        throw new Error('对象数据类型的值仅允许嵌套一层，比如：param.month');
        return;
      }

      if (!attributes.id) {
        throw new Error('触发的元素必须要绑定唯一的元素id');
        return;
      }

      new window.MonthDate({
        // 最小日期的元素id
        minEleId: '#' + (attributes.minEleId ? attributes.minEleId : ''),
        // 最大日期的元素id
        maxEleId: '#' + (attributes.maxEleId ? attributes.maxEleId : ''),
        // 月日触发的元素
        initEleId: '#' + attributes.id,
        // 符号 默认'-'
        symbol: attributes.symbol,
        // 选择天数的回调：点选天数与点击确定键
        ok: function (value, month, date) {
          var item;
          month = ('0' + month).slice(-2);
          date = ('0' + date).slice(-2);
          if (this.symbol == '月日') {
            item = month + '月' + date + '日';
          } else {
            item = month + this.symbol + date;
          }
          // 初始化嵌套的数据绑定
          if (list) {
            if (scope.$parent[list[0]]) {
              scope.$parent[list[0]][list[1]] = item;
            } else {
              scope[list[0]][list[1]] = item;
            }

          } else {
            if (scope.$parent[attributes.ngModel]) {
              scope.$parent[attributes.ngModel] = item;
            } else {
              scope[attributes.ngModel] = item;
            }
          }

          scope.$apply();
        },
        // 点击清空的回调：点击清空键
        reset: function () {
          if (list) {
            if (scope.$parent[list[0]]) {
              scope.$parent[list[0]][list[1]] = '';
            } else {
              scope[list[0]][list[1]] = '';
            }

          } else {
            if (scope.$parent[attributes.ngModel]) {
              scope.$parent[attributes.ngModel] = '';
            } else {
              scope[attributes.ngModel] = '';
            }

          }

          scope.$apply();
        },
        //
      }).init();
    }

    return {
      restrict: 'A',
      link: function (scope, element, attributes) {

        // 监听的目的为适用场景：初始化还没有生成dom节点，通过事件生成dom后在去初始化控件
        var time = setInterval(function () {
          if (document.querySelector('#' + attributes.id)) {
            initDate(scope, element, attributes);
            clearInterval(time);
          }
        }, 20);
      }
    };
  }

  monthDate.$inject = [];
  angular.module('app.directive.monthDate', []).directive('monthDate', monthDate);
});