# monthDate

#### 介绍
一款只能选择月日的日期控件，支持起止月日联动，支持链接符更换
在线演示地址：http://seebin.gitee.io/monthdate/monthDate.html

目前已运用于RequireJS + AngularJS框架，运用RequireJS引入月日控件库，再使用AngularJS创建一个指令来全局使用。（指令请参考AngularJS-Directive-monthDate.js文件）

由于在网上不好找这种特殊需求的控件，所以自己写了一个，目前是针对上面的框架做的，如果不能满足一部分的需求或含有bug，需要自己下载下来修改部分代码

#### 软件架构
Javascript


#### 安装教程

1. 引入css、js文件
2. 参照演示文件例子

#### 使用说明

具体使用方法，请参照monthDate.html例子文件。

`<input type="text" id="monthDate" readonly>`

```
var options = {
  // 初始化打开月日弹出框默认选中的月日值
  openInitMD: '09-01',
  // 月日触发的元素id，用于初始化月日控件，必传
  initEleId: '#moneyDate',
  // 用于联动的最小日期的元素id
  minEleId: '#minEleId',
  // 用于联动的最大日期的元素id
  maxEleId: '#maxEleId',
  // 是否点选日期关闭控件 默认true
  isAutoClose: true,
  // 链接符号,默认 '-',可取(- / 月日)，例如：'12-10'  '12/10'  '12月10日'
  symbol:'-',
  // 选择天数的回调：点选天数与点击确定键,在这里面回显选中的月日到视图，value为拼接好的字符串
  ok: function (value, month, date) {
      document.querySelector('#moneyDate').value = value;
  },
  // 点击清空的回调：点击清空键，在这里面清空视图显示的值
  reset: function () {
      document.querySelector('#moneyDate').value = '';
  },
}
// 初始化控件
new MonthDate(options).init();
```

